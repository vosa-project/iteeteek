#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Creating entries in /etc/hosts file..."

while read -r ip hostname; do

  if [ -n "$(grep $hostname /etc/hosts)" ]; then

    echo "Hostname '${hostname}' already exists in /etc/hosts"

  elif [ -n "$(grep $ip /etc/hosts)" ]; then

    echo "IP address '${ip}' already exists in /etc/hosts"

  else

    echo -e "\n# IP address for the lab ${hostname}" >> /etc/hosts
    echo -e "${ip}\t${hostname}" >> /etc/hosts
    echo "IP address '${ip}' for the host '${hostname}' was added to /etc/hosts"

    ssh-keygen -R $ip
    ssh-keygen -R $hostname

    ssh-keyscan -H $ip >> ~/.ssh/known_hosts
    ssh-keyscan -H $hostname >> ~/.ssh/known_hosts
fi
done< <(cat $DIR/config.json | jq -r '.host[] | "\(.ip) \(.hostname)"')
