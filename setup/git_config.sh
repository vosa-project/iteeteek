#!/bin/bash

echo "Saving git user details..."

git config --global user.email "olivertiks@gmail.com"
git config --global user.name "Oliver Tiks"

git config -l | grep -E 'user.*email|user.*name'

if [ $? -eq 0 ]; then
  echo "Done!"
else
  echo "Failed!"
fi
