#!/bin/bash

#apt-get install ruby hping3 -y &&
apt-get install --assume-yes python3-sphinx

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

bash "$DIR/setup/ip_hosts_setup.sh"
bash "$DIR/setup/git_config.sh"

bash "${DIR}/vta_setup.sh"
bash "$DIR/lab/service_setup.sh"
