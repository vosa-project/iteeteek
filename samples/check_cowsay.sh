#!/bin/bash

# Check if cowsay has been installed

LAST="false"

while true; do

        ssh root@server "dpkg -s cowsay" &> /dev/null

        RESULT=$?

        if [ $RESULT -ne 0 ] && [ $LAST == "false" ]; then
                echo "Cowsay not installed"
        elif [ $RESULT -eq 0 ] && [ $LAST == "false" ]; then
                echo "Cowsay has been installed"
                LAST="true"
        elif [ $RESULT -ne 0 ] && [ $LAST == "true" ]; then
                echo "Cowsay has been uninstalled"
                LAST="false"
        elif [ $RESULT -eq 0 ] && [ $LAST == "true" ]; then
                echo "Cowsay is still installed"
        else
                echo "Something went wrong!"
        fi

        sleep 5

done

