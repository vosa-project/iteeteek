#!/bin/bash

UNAME=$1

echo "ssh $2 '$3'"

while true; do

        ssh $2 $3 &> /dev/null

        RESULT=$?

        if [ $RESULT -eq 0 ]; then
                echo "Step '$1' completed!"
        else
                echo "Step '$1' failed!"
        fi

	sleep 5
done

