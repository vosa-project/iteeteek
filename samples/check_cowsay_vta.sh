#!/bin/bash

# Check if cowsay has been installed

. /opt/provisioned/lab_envs
. /opt/provisioned/functions

check_ssh $DESKTOP

OBJ="obj-y6x"
LAST="false"

while true; do

	ssh root@$DESKTOP "dpkg -s cowsay" &> /dev/null

	RESULT=$?

	if [ $RESULT -ne 0 ] && [ $LAST == "false" ]; then
		echo "Cowsay not installed"
	elif [ $RESULT -eq 0 ] && [ $LAST == "false" ]; then
		echo "Cowsay has been installed"
		LAST="true"
		obj_check $OBJ true
	elif [ $RESULT -ne 0 ] && [ $LAST == "true" ]; then
		echo "Cowsay has been uninstalled"
		LAST="false"
		obj_check $OBJ false
       	elif [ $RESULT -eq 0 ] && [ $LAST == "true" ]; then
		echo "Cowsay is still installed"
	else
		echo "Something went wrong!"
	fi

	sleep 5

done
