#!/bin/bash
# Script for checking lab objectives

# Author - Katrin Loodus
#
# Date - 27.04.2016
# Modify Date - xx.xx.201x
# Version - 0.0.x

LC_ALL=C

# set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script. Must be unique!
	CheckFile="/tmp/unique_name123"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP of server/desktop where test is performed. Must be set!
	IP_to_SSH=

	# Time to sleep between running the check again
	Sleep=5

	# Step uname in VirtualTA. Must be set!
	Uname=


}

# Test if action is successful

CHECKER () {

	while true
	do

   	# Check if user action is correct
    	ssh root@$IP_to_SSH 'echo "test here"'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nPackaged updated&upgraded!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Packages have not been upgraded! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

CHECKER

exit 0
