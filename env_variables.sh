#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Creating 'LAB_REPO' env variable..."
export LAB_REPO="$DIR"

env | grep "LAB_REPO"

if [ $? -eq 0 ]; then
        echo "Done!"
else
        echo "Failed!"
fi
