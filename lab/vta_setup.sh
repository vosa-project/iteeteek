#!/bin/bash

# Download lab vta data

_vtafile="/tmp/vta_data"

counter=10

while [ "${counter}" -gt 0  ]; do

  curl -fsH "accept: application/json" -X POST "$(dmidecode -s system-product-name)/labinfo?uuid=$(dmidecode -s system-version)" > "${_vtafile}"

  if [ "$?" -eq 0 ]; then
    break
  fi

  sleep 2
  $((counter--))

done

# Delete if empty lab data file
if [ ! -s "${_vtafile}" ]; then
  rm "${_vtafile}"
fi
