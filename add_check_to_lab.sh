#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

bash $DIR/functions

echo "Enter a unique name (uname) for the check to be created. No spaces!"
read UNAME

echo "Enter an ip address or a hostname"
read CLIENT

echo "Choose a category:"
ls $DIR/library
read CATEGORY

echo "Choose a check script:"
ls $DIR/library/$CATEGORY
read SCRIPT

echo "Enter argument(s)"
read ARGUMENTS

create_check "$UNAME" "$CLIENT $DIR/library/$CATEGORY/$SCRIPT" "$ARGUMENTS"
