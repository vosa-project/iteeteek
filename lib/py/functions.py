def package_exists(pname):
	return "dpkg -l " + str(pname)

def parttable_type(devname, ptype):
	return "fdisk -l " + str(devname) + " | grep \"Disklabel\" | grep " + str(ptype)

def user_exists(user):
	return "getent passwd " + str(user)

def group_exists(group):
	return "getent group " + str(group)
