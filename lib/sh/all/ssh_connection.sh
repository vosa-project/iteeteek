#!/bin/bash
#
# CATEGORY: ssh
#
# DESCRIPTION:
# Verifies the ssh connection for the specified user.
#
# ARGUMENTS:
# $1 - username
#
# SYNOPSIS:
# ssh_connection.sh [user]

user="$1"

run () {

  command="finger | grep --word-regexp ${user} | grep --word-regexp \"pts\""

  echo "${command}"

}

run
