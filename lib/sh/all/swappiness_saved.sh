#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies the active swap usage setting of the system.
#
# ARGUMENTS:
# $1 - swappiness value (integer)
#
# SYNOPSIS:
# swappiness_live.sh [$1]

value="$1"

run () {

  command="$(bash ./swappiness_active.sh ${value}) && "
  command+="grep vm.swappiness /etc/sysctl.conf | grep --invert-match \"#\" | grep --word-regexp ${value} && "
  command+="sysctl -p && "
  command+="$(bash ./swappiness_active.sh ${value})"

  echo "${command}"

}

run
