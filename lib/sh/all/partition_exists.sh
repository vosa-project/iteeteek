#!/bin/bash
#
# CATEGORY: disk drives
#
# DESCRIPTION:
# Verifies that an extended partition has been created on a block device, optionally checks partition number, id, and/or size.
#
# ARGUMENTS:
# $1 - device path without partition number
# ${@:2} - (optional) device path with partition number, size, id, and/or type
#
# SYNOPSIS:
# partition_extended.sh [device] [args]

device="$1"
args=("${@:2}")

run () {

  command="fdisk --list ${device}"
  command+="$(source ./functions && pipe_grep_arguments ${args[@]})"

  echo "${command}"

}

run
