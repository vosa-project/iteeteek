#!/bin/bash
#
# CATEGORY: disk drives
#
# DESCRIPTION:
# Searches for the type of the partition table for the specified block device.
#
# ARGUMENTS:
# $1 - device path
# $2 - partition table type
#
# SYNOPSIS:
# parttable_type.sh [device] [type]

device="$1"
type="$2"

run () {

  echo "fdisk --list ${device} | grep \"Disklabel\" | grep --word-regexp ${type}"

}

run
