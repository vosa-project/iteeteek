#!/bin/bash
#
# CATEGORY: disk drives
#
# DESCRIPTION:
# Verifies that the file system is mounted, optionally checks for file system type, total size, used size, available size, use percentage, and mount point.
#
# ARGUMENTS:
# $1 - device path
# ${@:2} - (optional) Filesystem Type, Total Size, Used Size, Available Size, Use%, Mountpoint
#
# SYNOPSIS:
# filesys_mounted.sh [device] [args]

device="$1"
args=("${@:2}")

run () {

  command="df --print-type --human-readable ${device}"
  command+="$(source ./functions && pipe_grep_arguments ${args[@]})"

#  for i in "${args[@]}"; do

#    command+=" | grep --word-regexp \"$i\""

#  done

  echo "${command}"

}

run
