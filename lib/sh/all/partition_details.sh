#!/bin/bash
#
# CATEGORY: disk drives
#
# DESCRIPTION:
# Verifies that the partition has been created, optionally checks label, size, file system, and mountpoint.
#
# ARGUMENTS:
# $1 - device path
# ${@:2} - (optional) file system type, partition size, mount point
#
# SYNOPSIS:
# partition_details.sh [device] [args]

device="$1"
args=("${@:2}")

run () {

  command="lsblk --output NAME,LABEL,SIZE,FSTYPE,MOUNTPOINT ${device}"
  command+="$(source ./functions && pipe_grep_arguments ${args[@]})"

#  for i in "${args[@]}"; do

#    command+=" | grep --word-regexp \"$i\""

#  done

  echo "${command}"

}

run
