#!/bin/bash
#
# CATEGORY: disk drives
#
# DESCRIPTION:
# Verifies the file system for the specified partition or volume.
#
# ARGUMENTS:
# $1 - device path
# $2 - file system type
#
# SYNOPSIS:
# filesys_type.sh [device] [type]

device="$1"
fstype="$2"

run () {

  command="blkid ${device} | grep TYPE=\\\\\"${fstype}\\\\\""

  echo "${command}"

}

run
