#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies that the specified swap area is turned on.
#
# ARGUMENTS:
# $1 - device or file path
#
# SYNOPSIS:
# swap_on.sh [$1]

path="$1"

run () {

  command="swapon --summary | grep \"${path}\|\\\$(readlink --canonicalize-existing ${path})\""
  echo "${command}"

}

run
