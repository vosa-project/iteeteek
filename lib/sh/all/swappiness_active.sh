#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies the active swap usage setting of the system.
#
# ARGUMENTS:
# $1 - swappiness value (integer)
#
# SYNOPSIS:
# swappiness_active.sh [$1]

value="$1"

run () {

  command="cat /proc/sys/vm/swappiness | grep --line-regexp ${value}"

  echo "${command}"

}

run
