#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies that the root session has been opened by the specified user.
#
# ARGUMENTS:
# $1 - username
#
# SYNOPSIS:
# sudo_privileges.sh [$1]

user="$1"

run () {

  command="grep \"session opened for user root by ${user}\" /var/log/auth.log"

  echo "${command}"

}

run
