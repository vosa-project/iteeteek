#!/bin/bash
#
# CATEGORY: users and groups
#
# DESCRIPTION:
# Searches the passwd database for key argument(s).
#
# ARGUMENTS:
# $@ - username or uid (multiple)
#
# SYNOPSIS:
# user_exists.sh [user]

user="$@"

run () {

  command="getent passwd ${user}"

  echo "${command}"

}

run
