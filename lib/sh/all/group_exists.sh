#!/bin/bash
#
# CATEGORY: users and groups
#
# DESCRIPTION:
# Searches the group database for key argument(s).
#
# ARGUMENTS:
# $@ - groupname or gid (multiple)
#
# SYNOPSIS:
# group_exists.sh [groupname / gid]

name="$@"

run () {

  command="getent group ${name}"

  echo "${command}"

}

run
