#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies that the specified swap area is turned on automatically.
#
# ARGUMENTS:
# $1 - device or file path
# $2 - mount point
#
# SYNOPSIS:
# swap_auto.sh [$1]

device="$1"
mountpoint="$2"

run () {

  command="$(bash ./fstab_linecheck.sh ${device} ${mountpoint}) && "
  command+="umount --no-mtab ${device} && mount --all --no-mtab"
  command+="$(bash ./filesys_mounted.sh ${device} ${mountpoint}) && "

  echo "${command}"

}

run
