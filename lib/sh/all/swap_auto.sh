#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies that the specified swap area is turned on automatically.
#
# ARGUMENTS:
# $1 - device or file path
#
# SYNOPSIS:
# swap_auto.sh [$1]

path="$1"

run () {

  command="$(bash ./fstab_linecheck.sh ${path} swap sw) && "
  command+="swapoff ${path} && swapon --all && "
  command+="$(bash ./swap_on.sh ${path})"

  echo "${command}"

}

run
