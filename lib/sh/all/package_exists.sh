#!/bin/bash
#
# CATEGORY: packages
#
# DESCRIPTION:
# Searches for packages matching given package-name-pattern
#
# ARGUMENTS:
# $@ - package name (multiple)
#
# SYNOPSIS:
# package_exists [name]

name="$@"

run () {

  command="dpkg --list ${name}"

  echo "${command}"

}

run
