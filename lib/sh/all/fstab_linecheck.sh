#!/bin/bash
#
# CATEGORY:
#
# DESCRIPTION:
# Verifies that the specified swap area is turned on automatically.
#
# ARGUMENTS:
# $1 - device or file path
# ${@:2} - (optional) <file system><mount point><type><options><dump><pass>
#
# SYNOPSIS:
# swap_auto.sh [$1]

path="$1"
args=("${@:2}")

run () {

  command="grep --word-regexp \"${path}\|\\\$(lsblk --noheadings --output UUID ${path})\" /etc/fstab | grep --invert-match \"#\""
  command+="$(source ./functions && pipe_grep_arguments ${args[@]})"

#  for i in "${args[@]}"; do

#    command+=" | grep --word-regexp \"$i\""

#  done

  echo "${command}"

}

run
