#!/bin/bash
#
# CATEGORY: users and groups
#
# DESCRIPTION:
# Verifies the default shell for the specified user.
#
# ARGUMENTS:
# $1 - username
# $2 - shell path
#
# SYNOPSIS:
# user_default_shell.sh [user] [shell]

user="$1"
shell="$2"

run () {

  command="getent passwd ${user} | grep ${shell}"

  echo "${command}"

}

run
