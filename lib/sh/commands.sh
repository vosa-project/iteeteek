#!/bin/bash

function package_exists {

	echo "dpkg -l $@";

}

function parttable_type {

	echo "fdisk -l $1 | grep \"Disklabel\" | grep -w $2";

}

function group_exists {

        echo "getent group $@";

}

function user_exists {

	echo "getent passwd $@";

}

function user_default_shell {

	echo "getent passwd $1 | grep $2";

}

function ssh_connection {

	echo "finger | grep $1 | grep pts";

}

function sudo_session {

	echo "grep \"session opened for user root by $1\" /var/log/auth.log"

}
